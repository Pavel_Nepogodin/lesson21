//
//  ViewController.swift
//  lesson21
//
//  Created by Harbros 74 on 15.08.21.
//

import UIKit

class ViewController: UIViewController {

    let contentView = View()

    override func viewDidLoad() {
        super.viewDidLoad()

        view = contentView
    }

}
